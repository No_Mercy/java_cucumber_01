$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("payment.feature");
formatter.feature({
  "line": 1,
  "name": "Payment testing",
  "description": "Automation test",
  "id": "payment-testing",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Login",
  "description": "",
  "id": "payment-testing;login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I input username  and password",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I click to Login button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I verify Homepage displayed \"Welcome To Manager\u0027s Page of Guru99 Bank\"",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.iInputUsernameAndPassword()"
});
formatter.result({
  "duration": 9578857093,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.iClickToLoginButton()"
});
formatter.result({
  "duration": 1978329053,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Welcome To Manager\u0027s Page of Guru99 Bank",
      "offset": 29
    }
  ],
  "location": "HomePageSteps.iVerifyHomepageDisplayed(String)"
});
formatter.result({
  "duration": 107377866,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Create new Customer and get NewCustomerID",
  "description": "",
  "id": "payment-testing;create-new-customer-and-get-newcustomerid",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@creatnewcustome"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I open New Customer page",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "Input data to all fields required",
  "rows": [
    {
      "cells": [
        "CustomerName",
        "DateOfBirth",
        "Address",
        "City",
        "State",
        "PIN",
        "Phone",
        "Email",
        "Password"
      ],
      "line": 14
    },
    {
      "cells": [
        "Huy",
        "06/06/1988",
        "Da Nang",
        "Da Nang",
        "DaNang",
        "222333",
        "094444",
        "huy",
        "123456"
      ],
      "line": 15
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Click to Submit button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Verify Customer created successfully with  message \"Customer Registered Successfully!!!\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Customer information should be shown",
  "rows": [
    {
      "cells": [
        "CustomerName",
        "Birthday",
        "Address",
        "City",
        "Phone"
      ],
      "line": 19
    },
    {
      "cells": [
        "Huy",
        "06/06/1988",
        "Da Nang",
        "Da Nang",
        "094444"
      ],
      "line": 20
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "NewCustomerStep.i_open_new_customer_page()"
});
formatter.result({
  "duration": 1761969765,
  "status": "passed"
});
formatter.match({
  "location": "NewCustomerStep.input_data_to_all_fields_required(DataTable)"
});
formatter.result({
  "duration": 1739985474,
  "status": "passed"
});
formatter.match({
  "location": "NewCustomerStep.click_to_submit_button()"
});
formatter.result({
  "duration": 2602117708,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Customer Registered Successfully!!!",
      "offset": 52
    }
  ],
  "location": "CustomerRegMsgPageSteps.verify_customer_created_successfully_with_message_something(String)"
});
formatter.result({
  "duration": 136144064,
  "status": "passed"
});
formatter.match({
  "location": "CustomerRegMsgPageSteps.customer_information_should_be_shown(DataTable)"
});
formatter.result({
  "duration": 581579998,
  "status": "passed"
});
});