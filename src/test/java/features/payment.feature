Feature: Payment testing
  Automation test

  @login
  Scenario: Login
    Given I input username  and password
    And I click to Login button
    Then I verify Homepage displayed "Welcome To Manager's Page of Guru99 Bank"

  @creatnewcustome
  Scenario: Create new Customer and get NewCustomerID
    Given I open New Customer page
    And Input data to all fields required
      | CustomerName | DateOfBirth | Address | City    | State | PIN    | Phone  | Email        | Password |
      | Huy          | 06/06/1988  | Da Nang | Da Nang | DaNang    | 222333 | 094444 | huy |   123456 |
    And Click to Submit button
    Then Verify Customer created successfully with  message "Customer Registered Successfully!!!"
    And Customer information should be shown
      | CustomerName | Birthday | Address      | City | Phone  | 
      | Huy          | 06/06/1988     | Da Nang | Da Nang | 094444 | 
    #And Get CustomerID for edit customer function
