package stepDefinitions;

import java.util.List;
import java.util.Map;
import java.util.List;
import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import junit.framework.Assert;
import pages.CustomerRegMsgPage;
import common.AbtractTest;

public class CustomerRegMsgPageSteps extends AbtractTest {
	WebDriver driver;
	private CustomerRegMsgPage customerRegMsgPage;
	private String regmsgName, regmsgdate, regmsgAdd, regmsgCity, regmsgPhone;

	public CustomerRegMsgPageSteps() {
		driver = Hooks.openBrowser();
		customerRegMsgPage = pages.PageFactory.getCustomerRegMsgPage(driver);

	}

	@Then("^Verify Customer created successfully with  message \"([^\"]*)\"$")
	public void verify_customer_created_successfully_with_message_something(String strArg1) throws Throwable {
		verifyTrue(customerRegMsgPage.customerPageIsDisplayed(strArg1));
	}

	

	@And("^Customer information should be shown$")
	public void customer_information_should_be_shown(DataTable tableUser) throws Throwable {
		regmsgName = customerRegMsgPage.getName();

		regmsgdate = customerRegMsgPage.getDate();
		regmsgAdd = customerRegMsgPage.getAdd();
		regmsgCity = customerRegMsgPage.getCity();
		regmsgPhone = customerRegMsgPage.getPhone();

		List<Map<String, String>> customerRegMsgInformation = tableUser.asMaps(String.class, String.class);

		verifyEquals(customerRegMsgInformation.get(0).get("CustomerName"), regmsgName);

		verifyEquals(customerRegMsgInformation.get(0).get("Address"), regmsgAdd);
		verifyEquals(customerRegMsgInformation.get(0).get("City"), regmsgCity);
		verifyEquals(customerRegMsgInformation.get(0).get("Phone"), regmsgPhone);

	}

}
