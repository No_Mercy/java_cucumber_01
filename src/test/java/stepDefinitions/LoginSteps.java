package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.AbtractTest;
import common.Constants;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;
import cucumberOption.*;

public class LoginSteps extends AbtractTest {
	WebDriver driver;
	WebDriverWait wait;

	private LoginPage loginPage;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;

	public LoginSteps() {
		driver = Hooks.openBrowser();
		loginPage = pages.PageFactory.getLoginPage(driver);
	}

	@When("^I input username  and password")
	public void iInputUsernameAndPassword() {
		// loginPage = new LoginPage(driver);
		loginPage.inputEmail(Constants.USERNAME);
		loginPage.inputPassword(Constants.PASSWORD);

	}

	@When("^I click to Login button$")
	public void iClickToLoginButton() {
		//homePage = new HomePage(driver);
		loginPage.clickSubmitButton();
		//newCustomerPage = homePage.openNewCustomerPage(driver);
		

	}
	
	/*@Then("^^I verify Homepage displayed \"(.*?)\"$")
	public void iVerifyHomepageDisplayed(String message) {
		verifyTrue(homePage.homePageIsDisplayed(message));

	}*/
	
	


    

}
