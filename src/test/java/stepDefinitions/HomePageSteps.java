package stepDefinitions;

import org.openqa.selenium.WebDriver;


import cucumber.api.java.en.Then;
import cucumberOption.Hooks;
import pages.HomePage;
import common.AbtractTest;

public class HomePageSteps extends AbtractTest{
	WebDriver driver;
	private HomePage homePage;
	
	 public HomePageSteps() {
		driver=Hooks.openBrowser();
		homePage=pages.PageFactory.getHomePage(driver);
		
	}
	
	
	@Then("^^I verify Homepage displayed \"(.*?)\"$")
	public void iVerifyHomepageDisplayed(String message) {
		//verifyTrue(homePage.homePageIsDisplayed(message));
		verifyTrue(homePage.homePageIsDisplayed(message));
		

	}
	

}
