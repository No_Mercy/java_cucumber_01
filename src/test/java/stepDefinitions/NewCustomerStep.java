package stepDefinitions;




import java.util.List;
import java.util.Map;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.AbtractTest;
import common.Constants;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;
import cucumberOption.*;

public class NewCustomerStep {
	WebDriver driver;
	WebDriverWait wait;
	private NewCustomerPage newCustomerPage;
	
	

	public NewCustomerStep() {
		driver = Hooks.openBrowser();
		newCustomerPage = pages.PageFactory.getNewCustomerPage(driver);
	}
	 public int randomNumber() {
		  Random rand = new Random(); 
		  int number = rand.nextInt(999999);
		  return number;
		 }

	@Given("^I open New Customer page$")
	public void i_open_new_customer_page() throws Throwable {
		newCustomerPage.openNewCustomerPage(driver);
	}

	@And("^Input data to all fields required$")
	public void input_data_to_all_fields_required(DataTable table) throws Throwable {
		
		List<Map<String,String>> customerInformation = table.asMaps(String.class, String.class);
		newCustomerPage.inputCustomerName(customerInformation.get(0).get("CustomerName"));
		newCustomerPage.inputDateOfBirth(customerInformation.get(0).get("DateOfBirth"));
		newCustomerPage.inputAddress(customerInformation.get(0).get("Address"));
		newCustomerPage.inputCity(customerInformation.get(0).get("City"));
		newCustomerPage.inputState(customerInformation.get(0).get("State"));
		newCustomerPage.inputPin(customerInformation.get(0).get("PIN"));
		newCustomerPage.inputPhone(customerInformation.get(0).get("Phone"));
		newCustomerPage.inputEmail(customerInformation.get(0).get("Email")+randomNumber()+"@mail.com");
		newCustomerPage.inputPass(customerInformation.get(0).get("Password"));
		

	}
	@And("^Click to Submit button$")
    public void click_to_submit_button() throws Throwable {
        newCustomerPage.clickSubmitButton();
    }
	


}
