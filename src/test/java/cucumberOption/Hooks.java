package cucumberOption;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import common.Constants;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	private static WebDriver driver;
	public static final Logger LOGGER = Logger.getLogger(Hooks.class.getName());

	@Before
	public synchronized static WebDriver openBrowser() {
		String browser = System.getProperty("BROWSER");

		if (driver == null) {
			try {
				if (browser == null) {
					browser = System.getenv("BROWSER");
					if (browser == null) {
						browser = "chrome";
					}
				}
				switch (browser) {
				case "chrome":
					System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
					driver = new ChromeDriver();
					break;
				case "firefox":
					driver = new FirefoxDriver();
					break;
				case "ie":
					System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
					driver = new InternetExplorerDriver();
					break;
				default:
					System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
					driver = new ChromeDriver();
					break;
				}
			} catch (UnreachableBrowserException e) {
				driver = new ChromeDriver();
			} catch (WebDriverException e) {
				driver = new ChromeDriver();
			} finally {
				Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
			}
			LOGGER.info("-----------------------START BROWSER---------------------------");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(Constants.BANK_GURU_URL);
		}
		return driver;
	}

	public static void close() {
		try {
			openBrowser().quit();
			driver = null;
			LOGGER.info("Closing the browser");
		} catch (UnreachableBrowserException e) {
			LOGGER.info("Can not close browser: unreachable browser");
		}
	}

	private static class BrowserCleanup implements Runnable {
		@Override
		public void run() {
			close();
		}
	}

	@After
	public void closeBroser() {
		try {
			if (driver != null) {
				driver.quit();
				System.gc();
				if (driver.toString().toLowerCase().contains("chrome")) {
					String cmd = "taskkill /IM chromedriver.exe /F";
					Process process = Runtime.getRuntime().exec(cmd);
					process.waitFor();
				}
				if (driver.toString().toLowerCase().contains("internetexplorerdriver")) {
					String cmd = "taskkill /IM IEDriverServer.exe /F";
					Process process = Runtime.getRuntime().exec(cmd);
					process.waitFor();
				}
				LOGGER.info("---------------------CLOSE BROWSER-------------------------");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info(e.getMessage());
		}
	}
}
