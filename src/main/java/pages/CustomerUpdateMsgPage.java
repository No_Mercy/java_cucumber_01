package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;

public class CustomerUpdateMsgPage extends Abstracstpage{
	WebDriver driver;
	public CustomerUpdateMsgPage(WebDriver driver){
		this.driver=driver;
	}
	
	public String getMsgsucess() {
		waitForControlVissible(driver,AbtractPageUI.CUSTOMER_UPDATE_MES);
		return getElementText(driver,AbtractPageUI.CUSTOMER_UPDATE_MES);

	}

}
