package pages;

import interfaces.*;
import common.Abstracstpage;

import org.openqa.selenium.WebDriver;




public class NewCustomerPage extends Abstracstpage{
	WebDriver driver;
	public NewCustomerPage(WebDriver driver){
		
		this.driver =driver;
	}
	public void inputCustomerName(String customername) {
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_NAME_PAGE);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_NAME_PAGE, customername);
		
	} 
	public void inputDateOfBirth(String dateofbirth){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_DATE_OF_BIRTH);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_DATE_OF_BIRTH, dateofbirth);
	}
	
	public void inputAddress(String address){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_ADDRESS);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_ADDRESS, address);
	}
	public void inputCity(String city){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_CITY);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_CITY, city);
		
	}
	public void inputState(String state){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_STATE);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_STATE, state);
	}
	public void inputPin(String pin){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_PIN);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_PIN, pin);
	}
	public void inputPhone(String phone){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_MOBI);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_MOBI, phone);
	}
	public void inputEmail(String cus_email){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_EMAIL);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_EMAIL, cus_email);
	}
	public void inputPass(String cus_pass){
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_PASS);
		sentKeyElement(driver, AbtractPageUI.CUSTOMER_PASS, cus_pass);
	}
	
	public CustomerRegMsgPage clickSubmitButton()
	{
		waitForControlVissible(driver, AbtractPageUI.CUSTOMER_SUBMIT_BTN);
		clickToElenment(driver, AbtractPageUI.CUSTOMER_SUBMIT_BTN);
		return PageFactory.getCustomerRegMsgPage(driver);
		
	}
}
