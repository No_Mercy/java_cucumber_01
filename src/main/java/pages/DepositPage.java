package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;




public class DepositPage extends Abstracstpage{
	WebDriver driver;
	
	public DepositPage(WebDriver driver){
		this.driver=driver;
	}
	public void inputAccNo(String accNo){
		waitForControlVissible(driver, AbtractPageUI.DEPOSIT_ACC_NO_TXT);
		sentKeyElement(driver, AbtractPageUI.DEPOSIT_ACC_NO_TXT, accNo);
	}
	
	public void inputAmount(String amount){
		waitForControlVissible(driver, AbtractPageUI.DEPOSIT_ACC_AMOUNT_TXT);
		sentKeyElement(driver, AbtractPageUI.DEPOSIT_ACC_AMOUNT_TXT, amount);
		}
	public void inputDes(String des){
		waitForControlVissible(driver, AbtractPageUI.DEPOSIT_DES_TXT);
		sentKeyElement(driver, AbtractPageUI.DEPOSIT_DES_TXT, des);
	}

}
