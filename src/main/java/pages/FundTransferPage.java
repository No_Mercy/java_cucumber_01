package pages;


import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;;

public class FundTransferPage extends Abstracstpage{
	WebDriver driver;
	public FundTransferPage(WebDriver driver){
		this.driver=driver;
	}
	public void inputAccNoFrom(String accNoFrom){
		waitForControlVissible(driver, AbtractPageUI.TRANS_ACC_TO_TXT);
		sentKeyElement(driver, AbtractPageUI.TRANS_ACC_TO_TXT, accNoFrom);
	}
	
	public void inputAccNoTo(String accNoTo){
		waitForControlVissible(driver, AbtractPageUI.TRANS_ACC_TO_TXT);
		sentKeyElement(driver, AbtractPageUI.TRANS_ACC_TO_TXT, accNoTo);
	}
	
	public void inputAmount(String amount){
		waitForControlVissible(driver, AbtractPageUI.TRANS_AMOUNT_TXT);
		sentKeyElement(driver, AbtractPageUI.TRANS_AMOUNT_TXT, amount);
		}
	public void inputDes(String des){
		waitForControlVissible(driver, AbtractPageUI.TRANS_DES_TXT);
		sentKeyElement(driver, AbtractPageUI.TRANS_DES_TXT, des);
	}
	
	public FundTransMsg clickToSubmit(){
		waitForControlVissible(driver, AbtractPageUI.TRANS_SUB_BTN);
		clickToElenment(driver, AbtractPageUI.TRANS_SUB_BTN);
		return new FundTransMsg(driver);
	}

}
