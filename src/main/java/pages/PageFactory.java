package pages;

import org.openqa.selenium.WebDriver;

public class PageFactory {
	private static HomePage homePage;
	private static RegisterPage registerPage;
	private static LoginPage loginPage;
	private static NewCustomerPage newCustomerPage;
	private static EditCustomerPage editCustomerPage;
	private static DeleteCustomerPage deleteCustomerPage;
	private static NewAccountPage newAccountPage;
	private static FundTransferPage fundTransferPage;
	private static BalanceEnquiryPage balanceEnquiryPage;

	public static LoginPage getLoginPage(WebDriver driver) {
		if (loginPage == null) {
			return new LoginPage(driver);
		}
		return loginPage;
	}

	public static RegisterPage getRegisterPage(WebDriver driver) {
		if (registerPage == null) {
			return new RegisterPage(driver);
		}
		return registerPage;
	}

	public static NewCustomerPage getNewCustomerPage(WebDriver driver) {
		if (newCustomerPage == null) {
			return new NewCustomerPage(driver);
		}
		return newCustomerPage;
	}

	public static HomePage getHomePage(WebDriver driver) {
		if (homePage == null) {
			return new HomePage(driver);
		}
		return homePage;
	}

	public static EditCustomerPage getEditCustomerPage(WebDriver driver) {
		if (editCustomerPage == null) {
			return new EditCustomerPage(driver);
		}
		return editCustomerPage;
	}

	public static DeleteCustomerPage getDeleteCustomerPage(WebDriver driver) {
		if (deleteCustomerPage == null) {
			return new DeleteCustomerPage(driver);
		}
		return deleteCustomerPage;
	}

	public static NewAccountPage getNewAccountPage(WebDriver driver) {
		if (newAccountPage == null) {
			return new NewAccountPage(driver);
		}
		return newAccountPage;
	}

	public static EditAccountPage getEditAccountPage(WebDriver driver) {
		return new EditAccountPage(driver);
	}

	public static DeleteAccountPage getDeleteAccountPage(WebDriver driver) {
		return new DeleteAccountPage(driver);
	}

	public static DepositPage getDepositPage(WebDriver driver) {
		return new DepositPage(driver);
	}

	public static WithdrawalPage getWithdrawalPage(WebDriver driver) {
		return new WithdrawalPage(driver);
	}

	public static FundTransferPage getFundTransferPage(WebDriver driver) {
		if (fundTransferPage == null) {
			return new FundTransferPage(driver);
		}
		return fundTransferPage;
	}

	public static ChangePasswordPage getChangePasswordPage(WebDriver driver) {
		return new ChangePasswordPage(driver);
	}

	public static BalanceEnquiryPage getBalanceEnquiryPage(WebDriver driver) {
		if (balanceEnquiryPage == null) {
			return new BalanceEnquiryPage(driver);
		}
		return balanceEnquiryPage;
	}

	public static MiniStatementPage getMiniStatementPage(WebDriver driver) {
		return new MiniStatementPage(driver);
	}

	public static CustomisedStatementPage getCustomisedStatementPage(WebDriver driver) {
		return new CustomisedStatementPage(driver);
	}

	public static CustomerRegMsgPage getCustomerRegMsgPage(WebDriver driver) {
		return new CustomerRegMsgPage(driver);
	}

	public static CustomerUpdateMsgPage getCustomerUpdateMsgPage(WebDriver driver) {
		return new CustomerUpdateMsgPage(driver);
	}

	public static InsertAccountMsgPage getInsertAccountMsgPage(WebDriver driver) {
		return new InsertAccountMsgPage(driver);
	}

	public static FundTransMsg getFundTransMsg(WebDriver driver) {
		return new FundTransMsg(driver);
	}
}
