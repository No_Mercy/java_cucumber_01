package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;;

public class InsertAccountMsgPage extends Abstracstpage{
	WebDriver driver;
	public InsertAccountMsgPage(WebDriver driver){
		this.driver=driver;
	}
	
	public String getMsgsucess() {
		waitForControlVissible(driver,AbtractPageUI.ACC_REG_MES);
		return getElementText(driver,AbtractPageUI.ACC_REG_MES);

	}
	public String getDeposit(){
		waitForControlVissible(driver,AbtractPageUI.ACC_REG_DEPOSIT);
		return getElementText(driver,AbtractPageUI.ACC_REG_DEPOSIT);
	}
	
	public String getAccNo(){
		waitForControlVissible(driver,AbtractPageUI.ACC_REG_NO);
		return getElementText(driver,AbtractPageUI.ACC_REG_NO);
	}

}
