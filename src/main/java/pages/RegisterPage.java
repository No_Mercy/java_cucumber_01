package pages;
import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;


public class RegisterPage extends Abstracstpage{
	WebDriver driver;
    
	
	public RegisterPage(WebDriver driver_registerpage){
		this.driver=driver_registerpage;
	}
	
	public void inputEmail(String emailValue) {
		waitForControlVissible(driver, RegisterPageUI.EMAIL_RG_TXT_ID);
		sentKeyElement(driver, RegisterPageUI.EMAIL_RG_TXT_ID, emailValue);

	}

	public void clickSubmitButton() {
		waitForControlVissible(driver, RegisterPageUI.LOGIN_RG_BTN);
		clickToElenment(driver, RegisterPageUI.LOGIN_RG_BTN);
	}

	public String getUserIDinfo() {
		waitForControlVissible(driver, RegisterPageUI.USER_ID_TXT);
		return getElementText(driver, RegisterPageUI.USER_ID_TXT);

	}

	public String getPassIDinfo() {
		waitForControlVissible(driver, RegisterPageUI.PASS_ID_TXT);
		return getElementText(driver, RegisterPageUI.PASS_ID_TXT);
		
	}

	public LoginPage openLoginPage(String url) {
		openAnyURL(driver, url);
		return new LoginPage(driver);
		
		
	}

	
	

}
