package pages;

import org.openqa.selenium.WebDriver;
import interfaces.*;
import common.Abstracstpage;;

public class LoginPage extends Abstracstpage{
	WebDriver driver;
	
	public LoginPage(WebDriver driver)
	{
		this.driver =driver;
	}

	public String  getLoginPageUrl() {
		 return getCurrentURL(driver);	
		
	}

	public RegisterPage clickHereLink() {
		waitForControlVissible(driver, LoginPageUI.HERE_LINK);
		clickToElenment(driver, LoginPageUI.HERE_LINK);
		return PageFactory.getRegisterPage(driver);
		
	}

	public void inputEmail(String username) {
		waitForControlVissible(driver, LoginPageUI.EMAIL_TXT);
		sentKeyElement(driver, LoginPageUI.EMAIL_TXT, username);
		
	}

	public void inputPassword(String userpass) {
		waitForControlVissible(driver, LoginPageUI.PASSWORD_TXT);
		sentKeyElement(driver, LoginPageUI.PASSWORD_TXT, userpass);
		
	}

	public HomePage clickSubmitButton() {
		waitForControlVissible(driver, LoginPageUI.LOGIN_BTN);
		clickToElenment(driver, LoginPageUI.LOGIN_BTN);
		return pages.PageFactory.getHomePage(driver);
		
	}

}
