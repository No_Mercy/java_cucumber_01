package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;


public class Edit_CustomerPage extends Abstracstpage{
	WebDriver driver;
	public Edit_CustomerPage(WebDriver driver){
		this.driver=driver;
	}
    
	public void updateAddress(String editAddress){
		waitForControlVissible(driver,AbtractPageUI.Edit_CUSTOMER_PAGE_ADDRESS);
		sentKeyElementEdit(driver, AbtractPageUI.Edit_CUSTOMER_PAGE_ADDRESS, editAddress);
		
	}
	
    public void updateCity(String editCity){
    	waitForControlVissible(driver,AbtractPageUI.Edit_CUSTOMER_PAGE_CITY);
		sentKeyElementEdit(driver, AbtractPageUI.Edit_CUSTOMER_PAGE_CITY, editCity);
		
	}
    
    public CustomerUpdateMsgPage clickSubmitButton(){
    	waitForControlVissible(driver, AbtractPageUI.Edit_CUSTOMER_SUB_BTN);
		clickToElenment(driver, AbtractPageUI.Edit_CUSTOMER_SUB_BTN);
		return PageFactory.getCustomerUpdateMsgPage(driver);
    }
    
    
}
