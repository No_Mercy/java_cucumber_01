package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;;


public class HomePage extends Abstracstpage{
	
WebDriver driver;
	
	public HomePage(WebDriver driver){
	
		this.driver =driver;
	}
	public void clickSubmitButton() {
		waitForControlVissible(driver, HomePageUI.LOGOUT_BTN);
		clickToElenment(driver, HomePageUI.LOGOUT_BTN);
	}
	
	public boolean homePageIsDisplayed(String message){
		waitForControlVissible(driver, HomePageUI.HOMEPAGE_LABLE, message);
		return isControlDisplayed(driver, HomePageUI.HOMEPAGE_LABLE, message);
	}
	
	public String getHomePageText()
	{
		return getElementText(driver, HomePageUI.HOMEPAGE_TEXT);
	}

}
