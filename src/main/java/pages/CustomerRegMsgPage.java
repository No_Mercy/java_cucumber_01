package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;
import interfaces.CustomerRegMsgUI;

public class CustomerRegMsgPage extends Abstracstpage{
	WebDriver driver;
	public CustomerRegMsgPage(WebDriver driver){
		this.driver=driver;
	}
	
	public boolean customerPageIsDisplayed(String message){
		waitForControlVissible(driver, CustomerRegMsgUI.CustomerRegMsg_LABLE, message);
		return isControlDisplayed(driver, CustomerRegMsgUI.CustomerRegMsg_LABLE, message);
	}
	
	public boolean customerNameIsDisplayed(String message){
		waitForControlVissible(driver, CustomerRegMsgUI.CUSTOMER_NAME, message);
		return isControlDisplayed(driver, CustomerRegMsgUI.CUSTOMER_NAME, message);
	}
	
	public String getName() {
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_NAME);
		return getElementText(driver,CustomerRegMsgUI.CUSTOMER_NAME);

	}
	public String getDate(){
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_BTH);
		return getElementText(driver,CustomerRegMsgUI.CUSTOMER_BTH);
	}
	
	public String getAdd(){
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_ADD);
		return getElementText(driver,CustomerRegMsgUI.CUSTOMER_ADD);
	}
	public String getCity(){
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_CITY);
		return getElementText(driver,CustomerRegMsgUI.CUSTOMER_CITY);
		
	}
	
	public String getPhone(){
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_PHONE);
		return getElementText(driver,CustomerRegMsgUI.CUSTOMER_PHONE);
		
	}
	/*public boolean customerNameIsDisplay(String message){
		waitForControlVissible(driver,CustomerRegMsgUI.CUSTOMER_NAME, message);
		return isControlDisplayed(driver, CustomerRegMsgUI.CUSTOMER_NAME, message);
	}*/
	

}
