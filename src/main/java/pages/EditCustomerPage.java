package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;




public class EditCustomerPage extends Abstracstpage{
	WebDriver driver;
	public EditCustomerPage(WebDriver driver){
		
		this.driver =driver;
	}
	public void inputCustID(String custID) {
		waitForControlVissible(driver,AbtractPageUI.Edit_CUSTOMER_ID_TXT);
		sentKeyElement(driver, AbtractPageUI.Edit_CUSTOMER_ID_TXT, custID);
		
	}
	public Edit_CustomerPage clickSubmitButton(){
		waitForControlVissible(driver,AbtractPageUI.Edit_CUSTOMER_SUBMIT_BTN);
		clickToElenment(driver, AbtractPageUI.Edit_CUSTOMER_SUBMIT_BTN);
		return new Edit_CustomerPage(driver);
	}

}
