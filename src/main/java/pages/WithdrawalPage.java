package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;


public class WithdrawalPage extends Abstracstpage{
	WebDriver driver;
	public WithdrawalPage(WebDriver driver){
		this.driver=driver;
	}
	public void inputAccNo(String accNo){
		waitForControlVissible(driver, AbtractPageUI.WITHDRAW_ACC_NO_TXT);
		sentKeyElement(driver, AbtractPageUI.WITHDRAW_ACC_NO_TXT, accNo);
	}
	
	public void inputAmount(String amount){
		waitForControlVissible(driver, AbtractPageUI.WITHDRAW_ACC_AMOUNT_TXT);
		sentKeyElement(driver, AbtractPageUI.WITHDRAW_ACC_AMOUNT_TXT, amount);
		}
	public void inputDes(String des){
		waitForControlVissible(driver, AbtractPageUI.WITHDRAW_DES_TXT);
		sentKeyElement(driver, AbtractPageUI.WITHDRAW_DES_TXT, des);
	}

}
