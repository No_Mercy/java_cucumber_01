package pages;

import org.openqa.selenium.WebDriver;

import interfaces.*;
import common.Abstracstpage;


public class NewAccountPage extends Abstracstpage{
	WebDriver driver;
	public NewAccountPage(WebDriver driver){
		this.driver=driver;
	}
    
	public void inputCustID(String custID) {
		waitForControlVissible(driver,AbtractPageUI.ADD_ACCOOUNT_CUSTOMERID);
		sentKeyElement(driver, AbtractPageUI.ADD_ACCOOUNT_CUSTOMERID, custID);
		
	}
	
	public void initialDeposit(String money) {
		waitForControlVissible(driver,AbtractPageUI.ADD_ACCOOUNT_DEPOSIT);
		sentKeyElement(driver, AbtractPageUI.ADD_ACCOOUNT_DEPOSIT, money);
		
	}
	public void selectAccountType(String Current){
		waitForControlVissible(driver,AbtractPageUI.ADD_ACCOOUNT_TYPE);
		selectIteminDropdown(driver, AbtractPageUI.ADD_ACCOOUNT_TYPE, Current);
	}
	
	public InsertAccountMsgPage clickSubmitAcc(){
		waitForControlVissible(driver,AbtractPageUI.ADD_ACCOOUNT_SUBMIT_BTN);
		clickToElenment(driver, AbtractPageUI.ADD_ACCOOUNT_SUBMIT_BTN);
		return new InsertAccountMsgPage(driver);
	}
}
