package interfaces;

public class CustomerRegMsgUI {
	public static final String CustomerRegMsg_LABLE="//p[contains(.,'Customer Registered Successfully!!!')]";
	public static final String CUSTOMER_NAME="(//td[contains(text(),'Customer Name')]/following-sibling::td)";
	public static final String CUSTOMER_BTH="(//td[contains(text(),'Birthdate')]/following-sibling::td)";
	public static final String CUSTOMER_ADD="(//td[contains(text(),'City')]/following-sibling::td)";
	public static final String CUSTOMER_CITY="(//td[contains(text(),'Address')]/following-sibling::td)";
	public static final String CUSTOMER_PHONE="(//td[contains(text(),'Mobile No.')]/following-sibling::td)";
	

}
