package interfaces;

public class AbtractPageUI {
	public static final String DYNAMIC_PAGES="//a[contains(text(),'%s')]";
	
    public static final String CUSTOMER_NAME_PAGE="//input[@name='name']";
    public static final String CUSTOMER_DATE_OF_BIRTH="//input[@id='dob']";
    public static final String CUSTOMER_ADDRESS="//textarea[@cols='20']";
    public static final String CUSTOMER_CITY="//input[@name='city']"; 
    public static final String CUSTOMER_STATE="//input[@name='state']";
    public static final String CUSTOMER_PIN="//input[@maxlength='6']";
    public static final String CUSTOMER_MOBI="//input[@name='telephoneno']";
    public static final String CUSTOMER_EMAIL="//input[@name='emailid']";
    public static final String CUSTOMER_PASS="//input[@name='password']";
    public static final String CUSTOMER_SUBMIT_BTN="//input[@type='submit']";
    
    public static final String CUSTOMER_REG_MES="//p[contains(.,'Customer Registered Successfully!!!')]";
    
    public static final String Edit_CUSTOMER_ID_TXT="//input[@name='cusid']";
    public static final String Edit_CUSTOMER_SUBMIT_BTN="//input[@name='AccSubmit']";
    
    public static final String Edit_CUSTOMER_PAGE_ADDRESS="//textarea[@cols='20']";
    public static final String Edit_CUSTOMER_PAGE_CITY="//input[@name='city']";
    public static final String Edit_CUSTOMER_SUB_BTN="//input[@name='sub']";
    
    public static final String CUSTOMER_UPDATE_MES="//p[@class='heading3']";
    
    public static final String ADD_ACCOOUNT_CUSTOMERID="//input[@name='cusid']"; 
    public static final String ADD_ACCOOUNT_TYPE="//select[@name='selaccount']";   
    public static final String ADD_ACCOOUNT_DEPOSIT="//input[@maxlength='8']";
    public static final String ADD_ACCOOUNT_SUBMIT_BTN="//input[@onclick='return validateAddAccount();']";
    public static final String ACC_REG_MES="//p[contains(.,'Account Generated Successfully!!!')]";
    public static final String ACC_REG_DEPOSIT="//td[contains(text(),'Current Amount')]/following-sibling::td";
    public static final String ACC_REG_NO="//td[contains(text(),'Account ID')]/following-sibling::td";
    
    public static final String DEPOSIT_ACC_NO_TXT="//input[@name='accountno']";
    public static final String DEPOSIT_ACC_AMOUNT_TXT="//input[@name='ammount']";
    public static final String DEPOSIT_DES_TXT="//input[@name='desc']";
    public static final String DEPOSIT_SUB_BTN="//input[@name='AccSubmit']";
    
    
    public static final String WITHDRAW_ACC_NO_TXT="//input[@name='accountno']";
    public static final String WITHDRAW_ACC_AMOUNT_TXT="//input[@name='ammount']";
    public static final String WITHDRAW_DES_TXT="//input[@name='desc']";
    public static final String WITHDRAW_SUB_BTN="//input[@name='AccSubmit']";
    
    
    public static final String TRANS_ACC_FROM_TXT="//input[@name='payersaccount']";
    public static final String TRANS_ACC_TO_TXT="//input[@name='payeeaccount']";
    public static final String TRANS_AMOUNT_TXT="//input[@name='ammount']";
    public static final String TRANS_DES_TXT="//input[@name='desc']";
    public static final String TRANS_SUB_BTN="//input[@name='AccSubmit']";
    
    
}
