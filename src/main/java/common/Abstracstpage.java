package common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import interfaces.*;
import pages.*;




public class Abstracstpage {
	WebDriver driver;
	private int timeout = 30;
	
	public void openAnyURL(WebDriver driver,String url){
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);

	}
	public String getTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public String getCurrentURL(WebDriver driver)
	{
		return driver.getCurrentUrl();
	}
    
	public void backToPage(WebDriver driver)
	{
		driver.navigate().back();
	}

	public void refreshAnyPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	public void clickToElenment(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}
	
	public void clickToElenment(WebDriver driver, String locator,String value) {
		String dynamicLocator =String.format(locator, value);
		WebElement element = driver.findElement(By.xpath(dynamicLocator));
		element.click();
	}

	public void sentKeyElement(WebDriver driver, String locator, String value) {
		WebElement element = driver.findElement(By.xpath(locator));
		
		element.sendKeys(value);
	}
	
	public void sentKeyElementEdit(WebDriver driver, String locator, String value) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.clear();
		element.sendKeys(value);
	}
	
	
	public void selectIteminDropdown(WebDriver driver, String locator, String value) {
		Select select = new Select(driver.findElement(By.xpath(locator)));
		select.selectByValue(value);

	}
	public String getFirstItemDropdown(WebDriver driver, String locator){
		Select select = new Select(driver.findElement(By.xpath(locator)));
		return select.getFirstSelectedOption().getText();
	}
	public String getAttributeElement(WebDriver driver, String locator,String attribute){
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attribute);
	}
	
	public String getElementText(WebDriver driver, String locator){
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getText();
		
	}
	public boolean isControlDisplayed(WebDriver driver, String locator){
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isDisplayed();
	}
	
	public boolean isControlDisplayed(WebDriver driver, String locator,String value){
		String dynamicLocator = String.format(locator, value);
		WebElement element = driver.findElement(By.xpath(dynamicLocator));
		return element.isDisplayed();
	}
	
	public boolean isControlSelected(WebDriver driver, String locator){
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}
	
	public void appectAlert(WebDriver drive){
		Alert alert = drive.switchTo().alert();
		alert.accept();
	}
    public void cancelAlert(WebDriver drive){
    	Alert alert = drive.switchTo().alert();
		alert.dismiss();
    }
    public String getTextAlert(WebDriver drive){
    	Alert alert = drive.switchTo().alert();
    	return alert.getText();
    }
    public void uploadFile(WebDriver driver, String locator,String filePath){
    	WebElement element = driver.findElement(By.xpath(locator));
    	element.sendKeys(filePath);
    }
    
    public void waitForControlVissible(WebDriver driver, String locator){
    	WebDriverWait wait = new WebDriverWait(driver, timeout);
    	By by = By.xpath(locator);
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    	
    }

	public void waitForControlVissible(WebDriver driver, String locator, String value) {
		String dynamicLocator = String.format(locator, value);
		System.out.println(dynamicLocator);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		By by = By.xpath(dynamicLocator);

		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    	
    }
    public void waitForControlInVissible(WebDriver driver, String locator){
    	WebDriverWait wait = new WebDriverWait(driver, timeout);
    	By by = By.xpath(locator);
    	
    	wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    	
    }
    public void waitForControlClickAble(WebDriver driver, String locator){
    	WebDriverWait wait = new WebDriverWait(driver, timeout);
    	By by = By.xpath(locator);
    	
    	wait.until(ExpectedConditions.elementToBeClickable(by));
    	
    }
    
    public NewCustomerPage openNewCustomerPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "New Customer");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "New Customer");
    	return PageFactory.getNewCustomerPage(driver);
    }
    public EditCustomerPage openEditCustomerPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Customer");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Customer");
    	return PageFactory.getEditCustomerPage(driver);
    }
    public DeleteCustomerPage openDeleteCustomerPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Delete Customer");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Delete Customer");
    	return PageFactory.getDeleteCustomerPage(driver);
    }
    
    public NewAccountPage openNewAccountPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "New Account");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "New Account");
    	return PageFactory.getNewAccountPage(driver);
    }
    public EditAccountPage openEditAccountPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Account");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Account");
    	return PageFactory.getEditAccountPage(driver);
    }
    public DeleteAccountPage openDeleteAccountPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Account");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Edit Account");
    	return PageFactory.getDeleteAccountPage(driver);
    }
    public DepositPage openDepositPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Deposit");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Deposit");
    	return PageFactory.getDepositPage(driver);
    }
    public WithdrawalPage openWithdrawalPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Withdrawal");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Withdrawal");
    	return PageFactory.getWithdrawalPage(driver);
    }
    public FundTransferPage openFundTransferPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Fund Transfer");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Fund Transfer");
    	return PageFactory.getFundTransferPage(driver);
    }
    public ChangePasswordPage openChangePasswordPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Change Password");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Change Password");
    	return PageFactory.getChangePasswordPage(driver);
    }
    public BalanceEnquiryPage openBalanceEnquiryPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Balance Enquiry");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Balance Enquiry");
    	return PageFactory.getBalanceEnquiryPage(driver);
    }
    public MiniStatementPage openMiniStatementPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Mini Statement");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Mini Statement");
    	return PageFactory.getMiniStatementPage(driver);
    }
    public CustomisedStatementPage openCustomisedStatementPage(WebDriver driver){
    	waitForControlVissible(driver, AbtractPageUI.DYNAMIC_PAGES, "Customised Statement");
    	clickToElenment(driver, AbtractPageUI.DYNAMIC_PAGES, "Customised Statement");
    	return PageFactory.getCustomisedStatementPage(driver);
    }
}
